public class ContainerRenter {
        private String name;
        private int container;
        private int totalCost;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getContainer() {
        return container;
    }

    public void setContainer(int container) {
        this.container = container;
    }

    public int getTotalCost() {
        return totalCost;
    }

    public ContainerRenter(String name, int container, int totalCost) {
        this.name = name;
        this.container = container;
        this.totalCost = totalCost;
    }
}
