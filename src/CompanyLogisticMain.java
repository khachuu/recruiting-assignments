import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CompanyLogisticMain {

    public static void main(String[] args) {
        List<ContainerRenter> containerRenterList = new ArrayList<>();
        ContainerRenter containerRenter1 = new ContainerRenter("Container renter A", 1, 1);
        ContainerRenter containerRenter2 = new ContainerRenter("Container renter B", 2, 2);
        ContainerRenter containerRenter3 = new ContainerRenter("Container renter C", 3, 1);
        ContainerRenter containerRenter4 = new ContainerRenter("Container renter D", 3, 1);
        ContainerRenter containerRenter5 = new ContainerRenter("Container renter F", 5, 3);
        ContainerRenter containerRenter6 = new ContainerRenter("Container renter G", 7, 3);
        containerRenterList.add(containerRenter1);
        containerRenterList.add(containerRenter2);
        containerRenterList.add(containerRenter3);
        containerRenterList.add(containerRenter4);
        containerRenterList.add(containerRenter5);
        containerRenterList.add(containerRenter6);

        // If we want to prioritize container renter has the highest number of container first, it will to sort by container
        containerRenterList = containerRenterList.stream().sorted(Comparator.comparing(ContainerRenter::getContainer).reversed()).collect(Collectors.toList());

        List<List<ContainerRenter>> containerRenters = selectContainerRenter(containerRenterList);

        int minCost = Integer.MAX_VALUE;
        int selectWayIndex = -1;
        for (int i = 0; i < containerRenters.size(); i++) {
            int cost = getCostFromListContainerRenter(containerRenters.get(i));
            if (cost < minCost) {
                minCost = cost;
                selectWayIndex = i;
            }
        }
        if (selectWayIndex == -1) {
            System.out.println("not enough containers");
        } else {
            for (ContainerRenter containerRenter: containerRenters.get(selectWayIndex)) {
                System.out.println("[Contract with] Container renter " + containerRenter.getName() + " " + containerRenter.getContainer() + " container, price: " + containerRenter.getTotalCost());
            }
            System.out.println("[Summary] total cost " + minCost);
        }
    }
    public static List<List<ContainerRenter>> selectContainerRenter(List<ContainerRenter> containerRenters) {
        List<List<ContainerRenter>> result = new ArrayList<>();
        process(containerRenters, 6, 0, new ArrayList<>(), result, 0);
        return result;
    }

    public static void process(List<ContainerRenter> containerRenters, int target, int currentSum, List<ContainerRenter> selectedContainerRenters, List<List<ContainerRenter>> result, int startIndex) {
        if (currentSum == target) {
            result.add(new ArrayList<>(selectedContainerRenters));
            return;
        }
        if (currentSum > target) {
            return;
        }

        for (int i = startIndex; i < containerRenters.size(); i++) {
            selectedContainerRenters.add(containerRenters.get(i));
            process(containerRenters, target, currentSum + containerRenters.get(i).getContainer(), selectedContainerRenters, result, i + 1);
            selectedContainerRenters.remove(selectedContainerRenters.size() - 1); // to backtrack
        }
    }

    private static int getCostFromListContainerRenter(List<ContainerRenter> containerRenters) {
        int cost = 0;
        for (ContainerRenter containerRenter: containerRenters) {
            cost += containerRenter.getTotalCost();
        }
        return cost;
    }
}